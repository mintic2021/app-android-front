package com.example.reservate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

public class RoomsActivity extends AppCompatActivity {
    //Rooms List
    TableLayout tdRooms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rooms);

        Button btn = (Button) findViewById(R.id.go_back);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), HomeAdmin.class);
                startActivityForResult(intent, 0);
            }
        });
    }
}