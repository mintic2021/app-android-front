package com.example.reservate;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.Notification;
import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class HomeAdmin extends AppCompatActivity {

    NotificationCompat.Builder notification;
    private static  final int idUnica = 51623;
    private EditText txtTipoHabitacion, txtCantidadHabitaciones,txtCantidadHuespeces,txtPrecio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_admin);

        txtTipoHabitacion = (EditText)findViewById(R.id.txtTipoHabitacion);
        txtCantidadHabitaciones = (EditText)findViewById(R.id.txtCantidadHabitaciones);
        txtCantidadHuespeces = (EditText)findViewById(R.id.txtCantidadHuespeces);
        txtPrecio = (EditText)findViewById(R.id.txtPrecio);

        Button btn = (Button) findViewById(R.id.enable_room);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), RoomsActivity.class);
                startActivityForResult(intent, 0);
            }
        });

        Button btn_save = (Button) findViewById(R.id.save_room);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                notification.setSmallIcon(R.mipmap.ic_launcher);
                notification.setTicker("Notificación");
                notification.setPriority(Notification.PRIORITY_HIGH);
                notification.setWhen(System.currentTimeMillis());
                notification.setContentTitle("Genial!");
                notification.setContentText("Éxito al guardar tu registro");

               // Intent intent = new Intent(HomeAdmin.this, HomeAdmin.class);

                //PendingIntent pendingIntent = PendingIntent.getActivity(HomeAdmin.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
               // notification.setContentIntent(pendingIntent);

                //NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                //nm.notify(idUnica, notification.build());

            }
        });
    }

    public void save_room(View view) {

        try {

            AdminSQLite admin = new AdminSQLite(this, "Administrar", null, 1);
            SQLiteDatabase BaseDatos = admin.getWritableDatabase();

            String tipo = txtTipoHabitacion.toString();
            int cantidad = Integer.parseInt(txtCantidadHabitaciones.getText().toString());
            int capacidad = Integer.parseInt(txtCantidadHabitaciones.getText().toString());
            double precio = Integer.parseInt(txtPrecio.getText().toString());

            ContentValues datos = new ContentValues();

            datos.put("tipo_habitacion", tipo);
            datos.put("cantidad_habitacion", cantidad);
            datos.put("capacidad_huespede", capacidad);
            datos.put("precio_habitacion", precio);

            BaseDatos.insert("habitacion", null, datos);
            BaseDatos.close();

            txtTipoHabitacion.setText("");
            txtCantidadHabitaciones.setText("");
            txtCantidadHabitaciones.setText("");
            txtPrecio.setText("");

            Toast.makeText(this, "Habitacion Registrado", Toast.LENGTH_LONG).show();


        } catch (Exception e) {

            Toast.makeText(this, "Error  " + e, Toast.LENGTH_LONG).show();

        }
    }

}