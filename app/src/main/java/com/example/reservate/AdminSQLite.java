package com.example.reservate;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class AdminSQLite extends SQLiteOpenHelper {

    public AdminSQLite(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase BaseDatos) {

        BaseDatos.execSQL("create table usuario(documento integer primary key ,nombre text, usuario text, password text )");
        BaseDatos.execSQL("create table habitacion (tipo_habitacion integer primary key, cantidad_habitacion int, capacidad_huespede int,precio_habitacion integer)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
