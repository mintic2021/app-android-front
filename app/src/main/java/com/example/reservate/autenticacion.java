package com.example.reservate;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class autenticacion extends AppCompatActivity {

    private EditText etusuario, etclave;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_autenticacion);

        etusuario = findViewById(R.id.etusuario);
        etclave = findViewById(R.id.etclave);
    }

    public void ingreso (View view){

        String usuario = etusuario.getText().toString();
        String clave = etclave.getText().toString();

        if (usuario.equals("admin") && clave.equals("12345")){
            AlertDialog.Builder dialogo2 = new AlertDialog.Builder(this);
            dialogo2.setTitle("Informacion");
            dialogo2.setMessage("Bienvenido al sistema");
            dialogo2.setPositiveButton("Aceptar", null);
            dialogo2.show();
            aceptar();

        }else{

            AlertDialog.Builder dialogo2 = new AlertDialog.Builder(this);
            dialogo2.setTitle("Informacion");
            dialogo2.setMessage("Validar usuario y contraseña");
            dialogo2.setPositiveButton("Aceptar", null);
            dialogo2.show();
        }
    }


    public void aceptar(){
        //Toast.makeText(this, "Bienvenido al sistema", Toast.LENGTH_LONG).show();
        Intent i = new Intent(this, HomeAdmin.class);
        startActivity(i);
    }

    public void cancelar(){
        finish();
    }

    public void Registrarse (View view){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Informacion");
        alert.setMessage("Sera redirecionado a la pagina de registro");
        alert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PntRegistro();
            }
        });
        alert.show();
    }

    public void PntRegistro(){
        Intent i = new Intent(this, RegistrationUser.class);
        startActivity(i);
    }

}