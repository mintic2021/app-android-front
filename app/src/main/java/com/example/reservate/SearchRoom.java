package com.example.reservate;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class SearchRoom extends AppCompatActivity {

    private static  final String TAG = "SearchRoom";
    private TextView txtfecha;
    private DatePickerDialog.OnDateSetListener mDateset;
    private TableLayout tblhabitaciones;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_room);

        txtfecha = (TextView)findViewById(R.id.txt_fecha);
        tblhabitaciones = (TableLayout)findViewById(R.id.tblhabitaciones);


        txtfecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        SearchRoom.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateset,
                        year, month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });

        mDateset = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                Log.d(TAG, "onDataSet: date: " + month + "/" + dayOfMonth + "/" + year );

                String date = month + "/" + dayOfMonth + "/" + year;

                txtfecha.setText(date);

            }
        };
    }

    public void llenarTabla(){
        AdminSQLite admin= new AdminSQLite( this, "Administrar", null ,1);
        SQLiteDatabase BaseDatos = admin.getWritableDatabase();

    }

    public void consultar (View view) {

        Toast.makeText(this,"No hay habitaciones disponibles", Toast.LENGTH_LONG).show();

    }
}