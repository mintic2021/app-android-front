package com.example.reservate;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class RegistrationUser extends AppCompatActivity {

    private TextView txt_name, txt_documento, txt_user, txt_password;
    boolean isAllFieldsChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_user);

        txt_documento = findViewById(R.id.txt_documento);
        txt_name = findViewById(R.id.txt_nombre);
        txt_user = findViewById(R.id.txt_user);
        txt_password = findViewById(R.id.txt_password);

    }

    public  void save_user(View view){

        try {

            isAllFieldsChecked = CheckAllFields();

            if (isAllFieldsChecked){

                AdminSQLite admin= new AdminSQLite( this, "Administrar", null ,1);
                SQLiteDatabase BaseDatos = admin.getWritableDatabase();

                String doc = txt_documento.toString();
                String nom = txt_name.toString();
                String user = txt_user.toString();
                String Password = txt_password.toString();

                ContentValues datos = new ContentValues();

                datos.put("documento", doc);
                datos.put("nombre", nom);
                datos.put("user",user);
                datos.put("pasword", Password);

                BaseDatos.insert("usuario", null, datos);
                BaseDatos.close();

                txt_documento.setText("");
                txt_name.setText("");
                txt_user.setText("");
                txt_password.setText("");

                Toast.makeText(this,"Usuario Registrado", Toast.LENGTH_LONG).show();

            }


        }catch (Exception e){

            Toast.makeText(this, "Error  " + e, Toast.LENGTH_LONG).show();
        }
    }

    private boolean CheckAllFields() {

        if (txt_documento.length() == 0) {
            txt_documento.setError("Este campo es requerido");
            return false;
        }

        if (txt_name.length() == 0) {
            txt_name.setError("Este campo es requerido");
            return false;
        }

        if (txt_user.length() == 0) {
            txt_user.setError("Este campo es requerido");
            return false;
        }

        if (txt_password.length() == 0) {
            txt_password.setError("Este campo es requerido");
            return false;
        } else if (txt_password.length() < 8) {
            txt_password.setError("El Password debe ser minimo de 8 carcteres");
            return false;
        }

        return true;
    }

    public void  cancel (View view){
        finish();
    }

}